import React from "react";
import { useEffect, useState } from "react";
import { Formik } from "formik";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";
import * as yup from "yup";
import io from "socket.io-client";
import "./ChatRoomPage.css";
import { getChatRoomMessages, getChatRooms } from "./requests";
const SOCKET_IO_URL = "https://blooming-everglades-96289.herokuapp.com";
const socket = io(SOCKET_IO_URL);

const getChatData = () => {
  return JSON.parse(localStorage.getItem("chatData"));
};

const schema = yup.object({
  message: yup.string().required("Message is required"),
});

function ChatRoomPage() {
  const [initialized, setInitialized] = useState(false);
  const [messages, setMessages] = useState([]);
  const [rooms, setRooms] = useState([]);

    const handleSubmit = async (evt, {resetForm}) => {
        const isValid = await schema.validate(evt);
        if (!isValid) {
            return;
        }
        const data = Object.assign({}, evt);
        data.chatRoomName = getChatData().chatRoomName;
        data.author = getChatData().handle;
        data.message = evt.message;
        socket.emit("message", data);
        resetForm({});
    };

    const connectToRoom = () => {
        socket.on("connect", data => {
            socket.emit("join", getChatData().chatRoomName);
        });

        socket.on("newMessage", data => {
            getMessages();
        });
        setInitialized(true);
    };

    const getMessages = async () => {
        const response = await getChatRoomMessages(getChatData().chatRoomName);
        setMessages(response.data);
        setInitialized(true);
    };

    const getRooms = async () => {
        const response = await getChatRooms();
        setRooms(response.data);
        setInitialized(true);
    };

    const formatData = (str) => {
        const date = new Date(str);
        return `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`
    };

    useEffect(() => {
        if (!initialized) {
            getMessages();
            connectToRoom();
            getRooms();
        }
    });

    return (
        <div className="chat-room-page">
            <h1>
                Название комнаты: {getChatData().chatRoomName}. Ваше имя:{" "}
                {getChatData().handle}
            </h1>
            <div className="chat-box">
                {messages.map((m, i) => {
                    return (
                        <div className="col-12" key={i}>
                            <div className="row">
                                <div className="col-2">{m.author}</div>
                                <div className="col">{m.message}</div>
                                <div className="col-3">{formatData(m.createdAt)}</div>
                            </div>
                        </div>
                    );
                })}
            </div>
            <Formik validationSchema={schema} onSubmit={handleSubmit}>
                {({
                      handleSubmit,
                      handleChange,
                      handleBlur,
                      values,
                      touched,
                      isInvalid,
                      resetForm,
                      errors,
                  }) => (
                    <Form noValidate onSubmit={e => {
                        handleSubmit(e, resetForm)}}>
                        <Form.Row>
                            <Form.Group as={Col} md="12" controlId="handle">
                                <Form.Label>Введите сообщение</Form.Label>
                                <Form.Control
                                    type="text"
                                    name="message"
                                    placeholder="Сообщение"
                                    value={values.message || ""}
                                    onChange={handleChange}
                                    isInvalid={touched.message && errors.message}
                                />
                                <Form.Control.Feedback type="invalid">
                                    {errors.message}
                                </Form.Control.Feedback>
                            </Form.Group>
                        </Form.Row>
                        <Button type="submit" style={{marginRight: "10px"}}>
                            Отправить
                        </Button>
                    </Form>
                )}
            </Formik>
        </div>
    );
}

export default ChatRoomPage;
